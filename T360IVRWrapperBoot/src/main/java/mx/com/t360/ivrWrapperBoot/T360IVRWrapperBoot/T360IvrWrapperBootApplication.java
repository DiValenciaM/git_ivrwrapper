package mx.com.t360.ivrWrapperBoot.T360IVRWrapperBoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T360IvrWrapperBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(T360IvrWrapperBootApplication.class, args);
	}

}
